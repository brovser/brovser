Brovser is licensed under the GNU Lesser General Public License, version 3.0 or
(at your option) any later version. You can read it in
[LICENSE-LGPL.md](LICENSE-LGPL.md).

Note that the LGPL text is only an extension to the GNU General Public License.
If you choose a particular LGPL version, you should read GPL text first (but
**the same version**). If you choose LGPL 3.0, corresponding GPL 3.0 text is
located in [LICENSE-GPL.md](LICENSE-GPL.md).
